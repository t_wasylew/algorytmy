package ciag_fibonacciego;

public class CiagFibonacciego {

    public static void main(String[] args) {

        System.out.println(fibonacci(15));
    }

    public static int fibonacci(int parametr) {

        if (parametr == 0) {
            return 0;
        } else if (parametr == 1) {
            return 1;
        } else {
            return fibonacci(parametr-1) + fibonacci(parametr-2);
        }
    }
}
