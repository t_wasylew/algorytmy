package hash_map;

import java.util.LinkedList;
import java.util.List;

public class MyHashMap {
    private List<List<MyEntry>> buckets = new LinkedList<>();
    private static final int bucketSize = 50;

    public MyHashMap() {
        for (int i = 0; i < bucketSize; i++) {
            buckets.add(new LinkedList<>());
        }
    }

//    public MyEntry iteratorList(String key) {
//        List<MyEntry> bucket = buckets.get(hash(key));
//        for (MyEntry e : bucket) {
//            if (e.getKey().equals(key)) {
//                return e;
//            }
//        }
//        return null;
//    }
    public Integer get(String key) {
        List<MyEntry> bucket = buckets.get(hash(key));
        for (MyEntry e : bucket) {
            if (e.getKey().equals(key)) {
                return e.getData();
            }
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public void put(String key, Integer data) {
        List<MyEntry> bucket = buckets.get(hash(key));
        for (MyEntry e : bucket) {
            if (e.getKey().equals(key)) {
                e.setData(data);
                return;
            }
        }

        bucket.add(new MyEntry(key, data));

    }

    private int hash(String key) {
        String[] splitted = key.split("");
        int sum = 0;
        for (String s : splitted) {
            sum += s.codePointAt(0);
        }

        return sum % bucketSize;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (List<MyEntry> list : buckets) {
            for (MyEntry e : list) {
                sb.append("[");
                sb.append(e.getKey());
                sb.append("=");
                sb.append(e.getData());
                sb.append("]");
            }
        }

        sb.append("}");
        return sb.toString();
    }
}
