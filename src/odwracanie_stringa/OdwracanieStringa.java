package odwracanie_stringa;

public class OdwracanieStringa {

    public static void main(String[] args) {
        String string = "QWERTYUIOP";
        System.out.println(reverse(string));
    }

    public static String reverse(String string) {
        if (string.length() == 1) {
            return string;
        }
        char znakZKonca = string.charAt(string.length() - 1);
        String stringBezOstatniegoZnaku = string.substring(0, string.length() - 1);
        return znakZKonca + reverse(stringBezOstatniegoZnaku);
    }

}
