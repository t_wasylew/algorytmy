package array_lista;

public class MyArrayList {

    private Object[] array;
    private int numberOfElements;

    public MyArrayList() {
        this.array = new Object[20000];
        this.numberOfElements = 0;
    }

    // dodaje 1 element
    // numberOfElements = 1
    // array[0] = element
    // array[1] = null

    public void addElement(Object data) {
        if (numberOfElements < array.length) {
            array[numberOfElements++] = data;
        } else {
            Object[] biggerArray = new Object[array.length * 2];
            for (int i = 0; i <= array.length; i++) {
                biggerArray[i] = array[i];
            }
            biggerArray[numberOfElements++] = data;
            array = biggerArray;
        }
        // sprawdz czy mamy jeszcze miejsce w tablicy
        // jesli mamy - umiesc
        // jesli nie mamy to stworz nowa tablice, przepisz elementy,
        //                                      i dopisz nowy element
    }


    public void removeElement(int index) {

        if (index >= numberOfElements) throw new ArrayIndexOutOfBoundsException();
        for (int i = index; i < numberOfElements; i++) {
            array[i] = array[i + 1];
        }
        numberOfElements--;
    }

    public void printList() {
        for (int i = 0; i < numberOfElements; i++) {
            System.out.println(array[i]);
        }
    }

    public void removeLast() {
        if (numberOfElements > 0) {
            numberOfElements--;
        }
    }

    public Object get(int indeks) {
        if (indeks >= numberOfElements) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return array[indeks];
    }

    public int size() {
        return numberOfElements;
    }
}
